import unittest
import json
import unittest.mock

from codeAPI.customExceptions import *
from codeAPI import userAPI


class BasicTests(unittest.TestCase):

	@unittest.mock.patch('codeAPI.userAPI.myoswrap')
	def test_hello(self, mock_oswrap):
		actual = userAPI.hello()
		self.assertIn('Welcome', actual)
		#check the three possible mock calls
		mock_oswrap.runCommandToAddUser.assert_not_called()
		mock_oswrap.runCommandToRemoveUser.assert_not_called()
		mock_oswrap.readFileByLine.assert_not_called()


	@unittest.mock.patch('codeAPI.userAPI.myoswrap')
	def test_delUser(self, mock_oswrap):
		#mocking a return value for one method (so the system appears to have 1 users overall)
		mock_oswrap.readFileByLine.return_value = ['Roy:x:1000:1000:Roy:/home/Roy:/bin/bash']
		obj = userAPI.delUser('Roy')
		#check the three possible mock calls
		mock_oswrap.readFileByLine.assert_called_with('/etc/passwd')
		mock_oswrap.runCommandToRemoveUser.assert_called_with('Roy')
		mock_oswrap.runCommandToAddUser.assert_not_called()

	@unittest.mock.patch('codeAPI.userAPI.myoswrap')
	def test_delUserNonExisting(self, mock_oswrap):
		with self.assertRaises(NonExistingUserException) as context:
			userAPI.delUser('Brodeur')
			#check the three possible mock calls
			mock_oswrap.runCommandToAddUser.assert_not_called()
			mock_oswrap.runCommandToRemoveUser.assert_not_called()
			mock_oswrap.readFileByLine.assert_called()

	@unittest.mock.patch('codeAPI.userAPI.myoswrap')
	def test_delInitialUser(self, mock_oswrap):
		with self.assertRaises(InitialUserException) as context:
			#mocking a return value for one method (so the system appears to have 1 users overall)
			mock_oswrap.readFileByLine.return_value = ['root:x:0:0:root:/home/root:/bin/bash']
			userAPI.delUser('root')
			#check the three possible mock calls
			mock_oswrap.runCommandToAddUser.assert_not_called()
			mock_oswrap.runCommandToRemoveUser.assert_not_called()
			mock_oswrap.readFileByLine.assert_called()

	@unittest.mock.patch('codeAPI.userAPI.myoswrap')
	def test_isInitialUser(self, mock_oswrap):
		actual = userAPI.isInitialUser('root')
		self.assertTrue(actual)

		mock_oswrap.runCommandToAddUser.assert_not_called()
		mock_oswrap.runCommandToRemoveUser.assert_not_called()
		mock_oswrap.readFileByLine.assert_not_called()

	@unittest.mock.patch('codeAPI.userAPI.myoswrap')
	def test_isNotInitialUser(self, mock_oswrap):
		actual = userAPI.isInitialUser('notroot')
		self.assertFalse(actual)

		mock_oswrap.runCommandToAddUser.assert_not_called()
		mock_oswrap.runCommandToRemoveUser.assert_not_called()
		mock_oswrap.readFileByLine.assert_not_called()

	@unittest.mock.patch('codeAPI.userAPI.myoswrap')
	def test_getUserList(self, mock_oswrap):
		mock_oswrap.readFileByLine.return_value = ['root:x:0:0:root:/home/root:/bin/bash']
		actual = userAPI.getUserList()
		self.assertIn('root', actual)

		mock_oswrap.readFileByLine.assert_called_with('/etc/passwd')
		mock_oswrap.runCommandToAddUser.assert_not_called()
		mock_oswrap.runCommandToRemoveUser.assert_not_called()

	@unittest.mock.patch('codeAPI.userAPI.myoswrap')
	def test_getLogs(self, mock_oswrap):
		mock_oswrap.readFileByLine.return_value = ['What a great log']
		actual = userAPI.getLogs()
		self.assertEqual(['What a great log'], actual)

		mock_oswrap.readFileByLine.assert_called_with('/var/log/auth.log')
		mock_oswrap.runCommandToAddUser.assert_not_called()
		mock_oswrap.runCommandToRemoveUser.assert_not_called()

	@unittest.mock.patch('codeAPI.userAPI.myoswrap')
	def test_getUsersWithOneOfEach(self, mock_oswrap):
		mock_oswrap.readFileByLine.return_value = ['root:x:0:0:root:/home/root:/bin/bash', 'test:x:0:0:test:/home/test:/bin/bash']
		actual = userAPI.getUsers()
		self.assertCountEqual(['root'], actual['InitialUsers'])
		self.assertCountEqual(['test'], actual['NewUsers'])

		mock_oswrap.readFileByLine.assert_called_with('/etc/passwd')
		mock_oswrap.runCommandToAddUser.assert_not_called()
		mock_oswrap.runCommandToRemoveUser.assert_not_called()

	@unittest.mock.patch('codeAPI.userAPI.myoswrap')
	def test_getUsersWithOnlyInitialUser(self, mock_oswrap):
		mock_oswrap.readFileByLine.return_value = ['root:x:0:0:root:/home/root:/bin/bash']
		actual = userAPI.getUsers()
		self.assertCountEqual(['root'], actual['InitialUsers'])
		self.assertCountEqual([], actual['NewUsers'])

		mock_oswrap.readFileByLine.assert_called_with('/etc/passwd')
		mock_oswrap.runCommandToAddUser.assert_not_called()
		mock_oswrap.runCommandToRemoveUser.assert_not_called()

	@unittest.mock.patch('codeAPI.userAPI.myoswrap')
	def test_getUsersWithOnlyNewUser(self, mock_oswrap):
		mock_oswrap.readFileByLine.return_value = ['test:x:0:0:test:/home/test:/bin/bash']
		actual = userAPI.getUsers()
		self.assertCountEqual([], actual['InitialUsers'])
		self.assertCountEqual(['test'], actual['NewUsers'])

		mock_oswrap.readFileByLine.assert_called_with('/etc/passwd')
		mock_oswrap.runCommandToAddUser.assert_not_called()
		mock_oswrap.runCommandToRemoveUser.assert_not_called()

	@unittest.mock.patch('codeAPI.userAPI.myoswrap')
	def test_resetUsers_onlyInitialUser(self, mock_oswrap):
		mock_oswrap.readFileByLine.return_value = ['root:x:0:0:root:/home/root:/bin/bash']
		actual = userAPI.resetUsers()
		self.assertEqual(0, actual)

		mock_oswrap.readFileByLine.assert_called_with('/etc/passwd')
		mock_oswrap.runCommandToAddUser.assert_not_called()
		mock_oswrap.runCommandToRemoveUser.assert_not_called()

	@unittest.mock.patch('codeAPI.userAPI.myoswrap')
	def test_resetUsers_onlyNewUser(self, mock_oswrap):
		mock_oswrap.readFileByLine.return_value = ['test:x:0:0:test:/home/test:/bin/bash']
		actual = userAPI.resetUsers()
		self.assertEqual(1, actual)

		mock_oswrap.readFileByLine.assert_called_with('/etc/passwd')
		mock_oswrap.runCommandToRemoveUser.assert_called_with('test')
		mock_oswrap.runCommandToAddUser.assert_not_called()

	@unittest.mock.patch('codeAPI.userAPI.myoswrap')
	def test_resetUsers_initialAndNewUser(self, mock_oswrap):
		mock_oswrap.readFileByLine.return_value = ['root:x:0:0:root:/home/root:/bin/bash', 'test:x:0:0:test:/home/test:/bin/bash', 'test1:x:0:0:test1:/home/test1:/bin/bash']
		actual = userAPI.resetUsers()
		self.assertEqual(2, actual)

		mock_oswrap.readFileByLine.assert_called_with('/etc/passwd')
		mock_oswrap.runCommandToRemoveUser.assert_called_with('test1')
		mock_oswrap.runCommandToAddUser.assert_not_called()

	@unittest.mock.patch('codeAPI.userAPI.myoswrap')
	def test_addUser(self, mock_oswrap):
		username = 'test123'
		userAPI.addUser(username)

		mock_oswrap.runCommandToAddUser.assert_called_with(username)
		mock_oswrap.runCommandToRemoveUser.assert_not_called()
		mock_oswrap.readFileByLine.assert_called()

	@unittest.mock.patch('codeAPI.userAPI.myoswrap')
	def test_addAlreadyExistingUser(self, mock_oswrap):
		with self.assertRaises(ExistingUserException) as context:
			mock_oswrap.readFileByLine.return_value = ['test:x:0:0:test:/home/test:/bin/bash']
			userAPI.addUser('test')

			mock_oswrap.readFileByLine.assert_called_with('/etc/passwd')
			mock_oswrap.runCommandToAddUser.assert_not_called()
			mock_oswrap.runCommandToRemoveUser.assert_not_called()

if __name__ == '__main__':
	unittest.main()

