import subprocess

def runCommandToAddUser(username):
	subprocess.call(["useradd", username])

def runCommandToRemoveUser(username):
	subprocess.call(["userdel", "-r", username])

def readFileByLine(filename):
	content = []
	file = open(filename, 'r')
	content = file.readlines()
	file.close
	return content
