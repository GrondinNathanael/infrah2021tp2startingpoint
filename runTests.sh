#!/bin/bash

python3 -m unittest discover -s project -p test_*.py -v
if [ $? -ne 0 ]
then
	echo "unittest failed, aborting"
	exit 1
fi
./runDocker.sh
sleep 2
python3 -m unittest discover -s project -p dtest_*.py -v
