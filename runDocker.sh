#!/bin/bash

docker stop tp2ng
docker container rm tp2ng
docker image rm tp2ng_image
docker volume rm tp2ng_vol

docker volume create --name tp2ng_vol --opt device=$PWD --opt o=bind --opt type=none
docker build -t tp2ng_image -f ./project/docker/Dockerfile .
docker run -d -p 5555:5555 --mount source=tp2ng_vol,target=/mnt/app/ --name tp2ng tp2ng_image
